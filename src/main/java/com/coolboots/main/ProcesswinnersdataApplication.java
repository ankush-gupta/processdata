package com.coolboots.main;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;


@SpringBootApplication
@ComponentScan("com.coolboots")
public class ProcesswinnersdataApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProcesswinnersdataApplication.class, args);
	}

}
