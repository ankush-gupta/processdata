package com.coolboots.sourcewise;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;

public class TestMainLamda3 {
private static final Logger log = LoggerFactory.getLogger(TestMain.class);
	
	@SuppressWarnings({ "null", "unlikely-arg-type" })
	public static void main(String[] args) {
		
		long startTime = System.currentTimeMillis();
		System.out.println(String.valueOf(startTime));
		
		String csvFile = "/var/www/html/sourcewisedata/" + "winners_2020_06_12.csv";
		
		
//++++++++++++++++++++Lambda function with group by++++++++++++++++++++++++++++++++++
		List<WinnerCsvDto> csvDataList;
		HashSet<String> userIdsSet = new HashSet<String>();
		
		List<GmvAndCashWonDto> gmvAndCashWonDtoList = new ArrayList<GmvAndCashWonDto>();
		
		//Read csv file 
		csvDataList = readCsvFile(csvFile);
		
		csvDataList.stream().forEach(e -> userIdsSet.add(e.getUser_id()));
		System.out.println(String.valueOf(userIdsSet.size()));
		
		Map<String,Integer> entryfee_sum = new HashMap<>();
		Map<String,Long> entryfee_count = new HashMap<>();
		
		Map<String,Integer> cash_sum = new HashMap<>();
		Map<String,Long> cash_count = new HashMap<>();
		
		entryfee_sum = csvDataList.stream().collect(Collectors.groupingBy(WinnerCsvDto::getUser_id,Collectors.summingInt(WinnerCsvDto::getEntry_fee)));
		entryfee_count = csvDataList.stream().filter(e -> e.getEntry_fee() != 0).collect(Collectors.groupingBy(WinnerCsvDto::getUser_id,Collectors.counting()));
		
		cash_sum = csvDataList.stream().collect(Collectors.groupingBy(WinnerCsvDto::getUser_id,Collectors.summingInt(WinnerCsvDto::getCash)));
		cash_count = csvDataList.stream().filter(e -> e.getCash() != 0).collect(Collectors.groupingBy(WinnerCsvDto::getUser_id,Collectors.counting()));
		
		System.out.println(cash_count.toString());
		System.out.println(entryfee_count.size());
		
		for(String userid : userIdsSet) {
			GmvAndCashWonDto gmvAndCashWonDto = new GmvAndCashWonDto();		
			
			gmvAndCashWonDto.setUser_id(Integer.parseInt(userid));
			
			gmvAndCashWonDto.setGmv_sum(entryfee_sum.get(userid));
			if(entryfee_count.containsKey(userid)) {
				gmvAndCashWonDto.setGmv_count(entryfee_count.get(userid));
			}
			else if(! entryfee_count.containsKey(userid)){
				gmvAndCashWonDto.setGmv_count(0);
			}
			
			gmvAndCashWonDto.setCash_sum(cash_sum.get(userid));
			if(cash_count.containsKey(userid)) {
				gmvAndCashWonDto.setCash_count(cash_count.get(userid));
			}
			else if(!cash_count.containsKey(userid)){
				gmvAndCashWonDto.setCash_count(0);
			}
			
			gmvAndCashWonDtoList.add(gmvAndCashWonDto);
			System.out.println(gmvAndCashWonDto.toString());			
		}
		
//		userIdsSet.forEach(userid -> {
//			GmvAndCashWonDto gmvAndCashWonDto = new GmvAndCashWonDto();
//			gmvAndCashWonDto.setUser_id(Integer.parseInt(userid));
//			gmvAndCashWonDto.setGmv_sum(entryfee_sum.get(userid));
//			gmvAndCashWonDto.setGmv_count(entryfee_count.get(userid).intValue());
//			gmvAndCashWonDto.setCash_sum(cash_sum.get(userid));
//			gmvAndCashWonDto.setCash_count(cash_count.get(userid).intValue());
//
//			gmvAndCashWonDtoList.add(gmvAndCashWonDto);
//		});		
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
		
//++++++++++++++++++++Lambda function normal++++++++++++++++++++++++++++++++++
//		List<WinnerCsvDto> csvDataList;
//		HashSet<String> userIdsSet = new HashSet<String>();
//		
//		List<GmvAndCashWonDto> gmvAndCashWonDtoList = new ArrayList<GmvAndCashWonDto>();
//		
//		//Read csv file 
//		csvDataList = readCsvFile(csvFile);
//		
//		csvDataList.stream().forEach(e -> userIdsSet.add(e.getUser_id()));
//		System.out.println(String.valueOf(userIdsSet.size()));
//		
//		
//		userIdsSet.forEach(userid -> {
//			GmvAndCashWonDto gmvAndCashWonDto = new GmvAndCashWonDto();
//			int entryFeeSum = 0;
//			int entryFeeCount = 0;
//			int cashSum = 0;
//			int cashCount = 0;
//
//			entryFeeSum = csvDataList.stream()
//					.filter(c -> c.getUser_id().equalsIgnoreCase(userid) && c.getEntry_fee() > 0)
//					.mapToInt(e -> e.getEntry_fee()).sum();
//			entryFeeCount = (int) csvDataList.stream()
//					.filter(c -> c.getUser_id().equalsIgnoreCase(userid) && c.getEntry_fee() > 0)
//					.mapToInt(e -> e.getEntry_fee()).count();
//
//			cashSum = csvDataList.stream().filter(c -> c.getUser_id().equalsIgnoreCase(userid) && c.getCash() > 0)
//					.mapToInt(e -> e.getCash()).sum();
//			cashCount = (int) csvDataList.stream()
//					.filter(c -> c.getUser_id().equalsIgnoreCase(userid) && c.getCash() > 0).mapToInt(e -> e.getCash())
//					.count();
//
//			gmvAndCashWonDto.setUser_id(Integer.parseInt(userid));
//			gmvAndCashWonDto.setGmv_sum(entryFeeSum);
//			gmvAndCashWonDto.setGmv_count(entryFeeCount);
//			gmvAndCashWonDto.setCash_sum(cashSum);
//			gmvAndCashWonDto.setCash_count(cashCount);
//
//			gmvAndCashWonDtoList.add(gmvAndCashWonDto);
//
//		});
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
		
//++++++++++++++++++++Core Java++++++++++++++++++++++++++++++++++		
//		List<WinnerCsvDto> csvDataList;
//		List<String> userIds = new ArrayList<String>();
//
//		GmvAndCashWonDto gmvAndCashWonDto;
//		List<GmvAndCashWonDto> gmvAndCashWonDtoList = new ArrayList<GmvAndCashWonDto>();
//		int entryFeeSum;
//		int entryFeeCount;
//		int cashSum;
//		int cashCount;
//
//		csvDataList = readCsvFile(csvFile);
//
//		for (WinnerCsvDto winnerCsvDto : csvDataList) {
//			if (!userIds.contains(winnerCsvDto.getUser_id())) {
//				userIds.add(winnerCsvDto.getUser_id());
//			}
//		}
//
//		for (String userId : userIds) {
//			gmvAndCashWonDto = new GmvAndCashWonDto();
//			entryFeeSum = 0;
//			entryFeeCount = 0;
//			cashSum = 0;
//			cashCount = 0;
//			for (WinnerCsvDto winnerCsvDto : csvDataList) {
//				if (userId.equalsIgnoreCase(winnerCsvDto.getUser_id())) {
//					if (winnerCsvDto.getEntry_fee() != 0) {
//						entryFeeSum = entryFeeSum + winnerCsvDto.getEntry_fee();
//						entryFeeCount++;
//					}
//					if (winnerCsvDto.getCash() != 0) {
//						cashSum = cashSum + winnerCsvDto.getCash();
//						cashCount++;
//					}
//				}
//			}
//			gmvAndCashWonDto.setUser_id(Integer.parseInt(userId));
//			gmvAndCashWonDto.setGmv_sum(entryFeeSum);
//			gmvAndCashWonDto.setGmv_count(entryFeeCount);
//			gmvAndCashWonDto.setCash_sum(cashSum);
//			gmvAndCashWonDto.setCash_count(cashCount);
//
//			gmvAndCashWonDtoList.add(gmvAndCashWonDto);
//		}
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
		
		long timeTaken = System.currentTimeMillis() - startTime;
		System.out.println(String.valueOf(timeTaken));
		
		System.out.println(gmvAndCashWonDtoList.size());
	}
	

//++++++++++++++++++++Read csv++++++++++++++++++++++++++++++++++
	private static List<WinnerCsvDto> readCsvFile(String csvfile) {
		WinnerCsvDto dto;
		List<WinnerCsvDto> dtoList = null;
	    CSVReader reader = null;
	    try {
	    	dtoList = new ArrayList<WinnerCsvDto>();
	        reader = new CSVReader(new FileReader(csvfile));
	        String[] line;
	        reader.readNext();
	        while ((line = reader.readNext()) != null) {
	        	dto = new WinnerCsvDto();
	        	
	        	dto.setChamp_state(Integer.parseInt(line[4].toString()));
	        	if(line[11].toString().equalsIgnoreCase("")) {
	        		dto.setEntry_fee(0);
	        	}else {
	        		dto.setEntry_fee(Integer.parseInt(line[11].toString()));
	        	}
	        	dto.setUser_id(line[12].toString());
	        	dto.setCash(Integer.parseInt(line[17].toString()));
	        	
	        	dtoList.add(dto);
	        }
	    } catch (IOException e) {
	    	log.error("Exception in ContestRewardAsyncService readCsvFile() : {}", e.getMessage());
	        e.printStackTrace();
	    }
	    return dtoList;
	}
}
