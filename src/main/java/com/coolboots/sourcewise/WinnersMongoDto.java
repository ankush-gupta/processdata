//package com.coolboots.sourcewise;
//
//import org.springframework.data.annotation.Id;
//import org.springframework.data.mongodb.core.mapping.Field;
//
//public class WinnersMongoDto {
//	@Id
//	@Field
//	private String id;
//	@Field
//	private int score;
//	@Field
//	private String gid;
//	@Field
//	private String user_id;
//	@Field
//	private String added_date;
//	@Field
//	private int rank;
//	@Field
//	private int tokens;
//	@Field
//	private long level_id;
//	@Field
//	private int entry_type;
//	@Field
//	private int champ_state;
//	@Field
//	private String display_text;
//	@Field
//	private int cash;
//	@Field
//	private String winning_amount;
//	@Field
//	private String tid;
//	@Field
//	private String created;
//	@Field
//	private String count;
//	@Field
//	private String title;
//	@Field
//	private int max_player;
//	@Field
//	private int entry_fee;
//	@Field
//	private int championship_type;
//	public String getId() {
//		return id;
//	}
//	public void setId(String id) {
//		this.id = id;
//	}
//	public int getScore() {
//		return score;
//	}
//	public void setScore(int score) {
//		this.score = score;
//	}
//	public String getGid() {
//		return gid;
//	}
//	public void setGid(String gid) {
//		this.gid = gid;
//	}
//	public String getUser_id() {
//		return user_id;
//	}
//	public void setUser_id(String user_id) {
//		this.user_id = user_id;
//	}
//	public String getAdded_date() {
//		return added_date;
//	}
//	public void setAdded_date(String added_date) {
//		this.added_date = added_date;
//	}
//	public int getRank() {
//		return rank;
//	}
//	public void setRank(int rank) {
//		this.rank = rank;
//	}
//	public int getTokens() {
//		return tokens;
//	}
//	public void setTokens(int tokens) {
//		this.tokens = tokens;
//	}
//	public long getLevel_id() {
//		return level_id;
//	}
//	public void setLevel_id(long level_id) {
//		this.level_id = level_id;
//	}
//	public int getEntry_type() {
//		return entry_type;
//	}
//	public void setEntry_type(int entry_type) {
//		this.entry_type = entry_type;
//	}
//	public int getChamp_state() {
//		return champ_state;
//	}
//	public void setChamp_state(int champ_state) {
//		this.champ_state = champ_state;
//	}
//	public String getDisplay_text() {
//		return display_text;
//	}
//	public void setDisplay_text(String display_text) {
//		this.display_text = display_text;
//	}
//	public int getCash() {
//		return cash;
//	}
//	public void setCash(int cash) {
//		this.cash = cash;
//	}
//	public String getWinning_amount() {
//		return winning_amount;
//	}
//	public void setWinning_amount(String winning_amount) {
//		this.winning_amount = winning_amount;
//	}
//	public String getTid() {
//		return tid;
//	}
//	public void setTid(String tid) {
//		this.tid = tid;
//	}
//	public String getCreated() {
//		return created;
//	}
//	public void setCreated(String created) {
//		this.created = created;
//	}
//	public String getCount() {
//		return count;
//	}
//	public void setCount(String count) {
//		this.count = count;
//	}
//	public String getTitle() {
//		return title;
//	}
//	public void setTitle(String title) {
//		this.title = title;
//	}
//	public int getMax_player() {
//		return max_player;
//	}
//	public void setMax_player(int max_player) {
//		this.max_player = max_player;
//	}
//	public int getEntry_fee() {
//		return entry_fee;
//	}
//	public void setEntry_fee(int entry_fee) {
//		this.entry_fee = entry_fee;
//	}
//	public int getChampionship_type() {
//		return championship_type;
//	}
//	public void setChampionship_type(int championship_type) {
//		this.championship_type = championship_type;
//	}
//	@Override
//	public String toString() {
//		return String.format(
//				"WinnersMongoDto [id=%s, score=%s, gid=%s, user_id=%s, added_date=%s, rank=%s, tokens=%s, level_id=%s, entry_type=%s, champ_state=%s, display_text=%s, cash=%s, winning_amount=%s, tid=%s, created=%s, count=%s, title=%s, max_player=%s, entry_fee=%s, championship_type=%s]",
//				id, score, gid, user_id, added_date, rank, tokens, level_id, entry_type, champ_state, display_text,
//				cash, winning_amount, tid, created, count, title, max_player, entry_fee, championship_type);
//	}
//
//}
