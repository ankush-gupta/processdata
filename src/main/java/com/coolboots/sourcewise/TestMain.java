package com.coolboots.sourcewise;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

public class TestMain {
	private static final Logger log = LoggerFactory.getLogger(TestMain.class);
	
	@SuppressWarnings("null")
	public static void main(String[] args) {
		long startTime = System.currentTimeMillis();
		//log.info(String.valueOf(startTime));
		
		String csvFile = "/var/www/html/sourcewisedata/" + "winners_2020_06_12.csv";
		
		String usersFile = "/var/www/html/sourcewisedata/" + "users_2020_06_12.csv";
		
//++++++++++++++++++++Lambda function with group by++++++++++++++++++++++++++++++++++
		List<WinnerCsvDto> csvDataList;
		//HashSet<String> userIdsSet = new HashSet<String>();
		
		List<GmvAndCashWonDto> gmvAndCashWonDtoList = new ArrayList<GmvAndCashWonDto>();
		
		List<Integer> userList;
		
		//Read csv file 
		csvDataList = readCsvFile(csvFile);
		//csvDataList.stream().forEach(e -> userIdsSet.add(e.getUser_id()));
		
		//read users list csv
		userList = readCsvFile2(usersFile);
		
		Map<String,Integer> entryfee_sum = new HashMap<>();
		Map<String,Long> entryfee_count = new HashMap<>();
		
		Map<String,Integer> cash_sum = new HashMap<>();
		Map<String,Long> cash_count = new HashMap<>();
		
		entryfee_sum = csvDataList.stream().collect(Collectors.groupingBy(WinnerCsvDto::getUser_id,Collectors.summingInt(WinnerCsvDto::getEntry_fee)));
		entryfee_count = csvDataList.stream().filter(e -> e.getEntry_fee() != 0).collect(Collectors.groupingBy(WinnerCsvDto::getUser_id,Collectors.counting()));
		
		cash_sum = csvDataList.stream().filter(e -> e.getEntry_type() == 1).collect(Collectors.groupingBy(WinnerCsvDto::getUser_id,Collectors.summingInt(WinnerCsvDto::getCash)));
		cash_count = csvDataList.stream().filter(e -> e.getCash() != 0 && e.getEntry_type() == 1).collect(Collectors.groupingBy(WinnerCsvDto::getUser_id,Collectors.counting()));
		
		for(Integer userid : userList) {
			
			if(entryfee_count.containsKey(userid.toString()) || cash_sum.containsKey(userid.toString())) {
				GmvAndCashWonDto gmvAndCashWonDto = new GmvAndCashWonDto();		
				
				gmvAndCashWonDto.setUser_id(userid);
				
				gmvAndCashWonDto.setGmv_sum(entryfee_sum.get(userid.toString()));
				if(entryfee_count.containsKey(userid.toString())) {
					gmvAndCashWonDto.setGmv_count(entryfee_count.get(userid.toString()));
				}
				else if(! entryfee_count.containsKey(userid.toString())){
					gmvAndCashWonDto.setGmv_count(0);
				}
				
				
				if(cash_sum.containsKey(userid.toString())) {
					gmvAndCashWonDto.setCash_sum(cash_sum.get(userid.toString()));
				}else {
					gmvAndCashWonDto.setCash_sum(0);
				}
				if(cash_count.containsKey(userid.toString())) {
					gmvAndCashWonDto.setCash_count(cash_count.get(userid.toString()));
				}
				else if(!cash_count.containsKey(userid.toString())){
					gmvAndCashWonDto.setCash_count(0);
				}
				gmvAndCashWonDtoList.add(gmvAndCashWonDto);
			}			
		}
		
		long timeTaken = System.currentTimeMillis() - startTime;
		log.info(String.valueOf("Time taken to process data :: "+timeTaken+"ms"));
		
		log.info("processed data size :: "+gmvAndCashWonDtoList.size());
		
		writeCsv(gmvAndCashWonDtoList);
		
	}
	


//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++	
	
	//write csv
	private static void writeCsv(List<GmvAndCashWonDto> gmvAndCashWonDtoList) {
	    File file = new File("/var/www/html/sourcewisedata/"+"output"+String.valueOf(System.currentTimeMillis())+".csv"); 
	    try { 
	        FileWriter outputfile = new FileWriter(file); 
	        CSVWriter writer = new CSVWriter(outputfile); 
	        // adding header to csv 			        
	        String[] header = { "user_id", "gmv_sum", "gmv_count", "cash_sum", "cash_count"};
	        writer.writeNext(header); 
	  
	        for(GmvAndCashWonDto item :gmvAndCashWonDtoList) {
	        	if(item.getGmv_sum() != 0 && item.getGmv_count() != 0 && item.getCash_sum() != 0 && item.getCash_count() != 0) {
	        		String[] data = { String.valueOf(item.getUser_id()) , String.valueOf(item.getGmv_sum()), String.valueOf(item.getGmv_count()), String.valueOf(item.getCash_sum()), String.valueOf(item.getCash_count()) }; 
		        	 writer.writeNext(data,false);
	        	}       	
	        }	 
	        writer.close(); 
	        
		} catch (IOException e) {
			log.error("Exception in ContestRewardAsyncService writeCsv() : {}", e.getMessage());
	        e.printStackTrace();
		}
	}

	// read csv file 
	private static List<WinnerCsvDto> readCsvFile(String csvfile) {
		WinnerCsvDto dto;
		List<WinnerCsvDto> dtoList = null;
	    CSVReader reader = null;
	    try {
	    	dtoList = new ArrayList<WinnerCsvDto>();
	        reader = new CSVReader(new FileReader(csvfile));
	        String[] line;
	        reader.readNext();
	        while ((line = reader.readNext()) != null) {
	        	dto = new WinnerCsvDto();
	        	
	        	dto.setChamp_state(Integer.parseInt(line[4].toString()));
	        	if(line[11].toString().equalsIgnoreCase("")) {
	        		dto.setEntry_fee(0);
	        	}else {
	        		dto.setEntry_fee(Integer.parseInt(line[11].toString()));
	        	}
	        	dto.setUser_id(line[12].toString());
	        	dto.setEntry_type(Integer.parseInt(line[16].toString()));
	        	dto.setCash(Integer.parseInt(line[17].toString()));
	        	
	        	dtoList.add(dto);
	        }
	    } catch (IOException e) {
	    	log.error("Exception in ContestRewardAsyncService readCsvFile() : {}", e.getMessage());
	        e.printStackTrace();
	    }
	    return dtoList;
	}
	
	//read csv file 2	
	private static List<Integer> readCsvFile2(String usersFile) {
		 List<Integer> usersList = null;
		 CSVReader reader = null;
		 try {
		    	usersList = new ArrayList<Integer>();
		        reader = new CSVReader(new FileReader(usersFile));
		        String[] line;
		        reader.readNext();
		        while ((line = reader.readNext()) != null) {		        	
		        	usersList.add(Integer.parseInt(line[0].toString()));
		        }
		    } catch (IOException e) {
		    	log.error("Exception in ContestRewardAsyncService readCsvFile2() : {}", e.getMessage());
		        e.printStackTrace();
		    }
		 return usersList;
	}
}
