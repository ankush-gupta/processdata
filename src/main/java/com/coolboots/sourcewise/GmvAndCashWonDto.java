package com.coolboots.sourcewise;

public class GmvAndCashWonDto {
	private int user_id;
	private int gmv_sum;
	//private int gmv_count;
	private long gmv_count;
	
	private int cash_sum;
	//private int cash_count;
	private long cash_count;
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public int getGmv_sum() {
		return gmv_sum;
	}
	public void setGmv_sum(int gmv_sum) {
		this.gmv_sum = gmv_sum;
	}
	public long getGmv_count() {
		return gmv_count;
	}
	public void setGmv_count(long gmv_count) {
		this.gmv_count = gmv_count;
	}
	public int getCash_sum() {
		return cash_sum;
	}
	public void setCash_sum(int cash_sum) {
		this.cash_sum = cash_sum;
	}
	public long getCash_count() {
		return cash_count;
	}
	public void setCash_count(long cash_count) {
		this.cash_count = cash_count;
	}
	@Override
	public String toString() {
		return String.format("GmvAndCashWonDto [user_id=%s, gmv_sum=%s, gmv_count=%s, cash_sum=%s, cash_count=%s]",
				user_id, gmv_sum, gmv_count, cash_sum, cash_count);
	}
	
	

}
