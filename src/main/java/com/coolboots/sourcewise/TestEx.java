package com.coolboots.sourcewise;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class TestEx {

	public static void main(String args[]) throws Exception {
		
		Map<String,Integer> userGroup_amount = new HashMap<>();
		Map<String,Long> userGroup_count = new HashMap<>();
		
		List<Tmodel> listTmodel = new ArrayList<Tmodel>();

		listTmodel.add(new Tmodel("1", 10, 100));
		listTmodel.add(new Tmodel("1", 20, 200));
		listTmodel.add(new Tmodel("2", 10, 100));
		listTmodel.add(new Tmodel("3", 10, 100));
		listTmodel.add(new Tmodel("3", 30, 300));
		
		
		/*	foo -> foo.id,
        Collectors.summingInt(foo->foo.targetCost))
*/
	//	userGroup=listTmodel.stream().collect(Collectors.groupingBy(foo->foo.ge));
		
		
		userGroup_amount= listTmodel.stream().collect(Collectors.groupingBy(Tmodel::getUserId,Collectors.summingInt(Tmodel::getAmount)));
		userGroup_count= listTmodel.stream().collect(Collectors.groupingBy(Tmodel::getUserId,Collectors.counting()));
		
		for (Map.Entry<String,Long> ugr : userGroup_count.entrySet())  
            System.out.println("Key = " + ugr.getKey() + 
                             ", Value = " + ugr.getValue()); 
		
		for (Entry<String, Integer> ugr : userGroup_amount.entrySet())  
            System.out.println("Key = " + ugr.getKey() + 
                             ", Value = " + ugr.getValue());
		
		
		
		

	}

}
