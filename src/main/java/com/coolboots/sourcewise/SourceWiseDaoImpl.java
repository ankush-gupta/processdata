//package com.coolboots.sourcewise;
//
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Map;
//
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Qualifier;
//import org.springframework.data.mongodb.core.MongoTemplate;
//import org.springframework.data.mongodb.core.aggregation.Aggregation;
//import org.springframework.data.mongodb.core.aggregation.AggregationResults;
//import org.springframework.data.mongodb.core.aggregation.ConditionalOperators;
//import org.springframework.data.mongodb.core.aggregation.ConditionalOperators.Cond;
//import org.springframework.data.mongodb.core.aggregation.GroupOperation;
//import org.springframework.data.mongodb.core.aggregation.MatchOperation;
//import org.springframework.data.mongodb.core.aggregation.ProjectionOperation;
//import org.springframework.data.mongodb.core.query.Criteria;
//import org.springframework.stereotype.Repository;
//
//import com.games.util.Constants;
//
//@Repository
//public class SourceWiseDaoImpl implements SourceWiseDao {
//	
//	private static final Logger log = LoggerFactory.getLogger(SourceWiseDaoImpl.class);
//	
//	@Autowired
//	@Qualifier("mongoTemplateReplica")
//	private MongoTemplate mongoTemplateReplica;
//
//	@Override
//	public List<GmvAndCashWonDto> getUserIds(String date) {
//		try {
//			
//			List<GmvAndCashWonDto> gmvAndCashWonDtoList = new ArrayList<>();
//			GmvAndCashWonDto gmvAndCashWonDto;
//			
//			String collection = Constants.GAME_WINNERS_COLLECTION + date;
//			System.out.println(collection);
//			
//			//ProjectionOperation project = Aggregation.project("totalEntryFees", "totalCashWon");
//			
//			MatchOperation match = Aggregation.match(Criteria.where("champ_state").is(4));	
//			
////			Cond cond_entryfees = ConditionalOperators.when(Criteria.where("champ_state").is(4))
////					.thenValueOf("$entry_fee").otherwise(0);
////			
////			Cond cond_cashwon = ConditionalOperators.when(Criteria.where("champ_state").is(4))
////					.thenValueOf("$cash").otherwise(0);
//			
//			Cond cond = ConditionalOperators.when(Criteria.where("champ_state").is(4)).then(1).otherwise(0);
//			
//			GroupOperation group = Aggregation.group("user_id","gid").sum("entry_fee").as("totalEntryFees").count().as("totalGamePlayedCount")
//					.sum("cash").as("totalCashWon").count().as("totalCashWonCount");
//			
//			Aggregation aggregation = Aggregation.newAggregation(group,match);
//			
//			log.info("aggregation::" + aggregation);
//			
//			AggregationResults<Map> totalResults = mongoTemplateReplica.aggregate(aggregation,collection, Map.class);
//			
//			for (Map mapResult : totalResults) {
//				gmvAndCashWonDto = new GmvAndCashWonDto();
//
//				gmvAndCashWonDto.setUser_id(Integer.parseInt(mapResult.get("user_id").toString()));
//				gmvAndCashWonDto.setGid(Integer.parseInt(mapResult.get("gid").toString()));
//				
//				if (mapResult.containsKey("totalEntryFees")) {
//					gmvAndCashWonDto.setGmv_sum(Integer.parseInt(mapResult.get("totalEntryFees").toString()));
//				}
//				gmvAndCashWonDto.setGmv_count(Integer.parseInt(mapResult.get("totalGamePlayedCount").toString()));
//
//				if (mapResult.containsKey("totalCashWon")) {
//					gmvAndCashWonDto.setCash_sum(Integer.parseInt(mapResult.get("totalCashWon").toString()));
//				}
//				gmvAndCashWonDto.setCash_count(Integer.parseInt(mapResult.get("totalCashWonCount").toString()));
//				
//				gmvAndCashWonDtoList.add(gmvAndCashWonDto);
//
//			}
//			
//			//log.info("GmvAndCashWonDtoList :: "+gmvAndCashWonDtoList.toString());
//			
//			return gmvAndCashWonDtoList;
//		} catch (Exception e) {
//			log.error("exception in SourceWiseDaoImpl getUserIds",e.getMessage());
//			e.printStackTrace();
//		}
//		return null;
//	}
//}
