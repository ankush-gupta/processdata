package com.coolboots.sourcewise;
public class Tmodel {

	public Tmodel(String userId, int gid, int amount) {

		this.userId = userId;
		this.gid = gid;
		this.amount = amount;

	}

	private String userId;
	private int amount;
	private int gid;

	public int getGid() {
		return gid;
	}

	public void setGid(int gid) {
		this.gid = gid;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
