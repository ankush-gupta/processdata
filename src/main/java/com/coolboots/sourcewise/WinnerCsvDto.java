package com.coolboots.sourcewise;

public class WinnerCsvDto {
	
	
//	private int score;
//	
//	private String gid;
	
	private String user_id;
	
//	private String added_date;
//	
//	private int rank;
//	
//	private int tokens;
//	
//	private long level_id;
	
	private int entry_type;
	
	private int champ_state;
	
//	private String display_text;
	
	private int cash;
	
//	private String winning_amount;
//	
//	private String tid;
//	
//	private String created;
//	
//	private String count;
//	
//	private String title;
//	
//	private int max_player;
	
//	private int championship_type;
	
	private int entry_fee;

	public String getUser_id() {
		return user_id;
	}

	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	public int getEntry_type() {
		return entry_type;
	}

	public void setEntry_type(int entry_type) {
		this.entry_type = entry_type;
	}

	public int getChamp_state() {
		return champ_state;
	}

	public void setChamp_state(int champ_state) {
		this.champ_state = champ_state;
	}

	public int getCash() {
		return cash;
	}

	public void setCash(int cash) {
		this.cash = cash;
	}

	public int getEntry_fee() {
		return entry_fee;
	}

	public void setEntry_fee(int entry_fee) {
		this.entry_fee = entry_fee;
	}

	@Override
	public String toString() {
		return String.format("WinnerCsvDto [user_id=%s, entry_type=%s, champ_state=%s, cash=%s, entry_fee=%s]", user_id,
				entry_type, champ_state, cash, entry_fee);
	}

	

	
}
